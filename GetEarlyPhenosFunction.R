getearlydata1 = function(earlyphenos, getrep="1"){
  i = 1
  for(i in i:length(earlyphenos)){
    if(names(earlyphenos[i]) == "CTD"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030613" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "Chla"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030813" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "HTPheight"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030613" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "NDRE"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030813" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "NDVI"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030413" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "NIR_760"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030613" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "R_670"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030613" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "RE_730"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030613" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
  }
  return(earlyphenos)
}

getearlydata2 = function(earlyphenos, getrep="2"){
  i = 1
  for(i in i:length(earlyphenos)){
    if(names(earlyphenos[i]) == "CTD"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "031513" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "Chla"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "040313" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "HTPheight"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "031413" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "NDRE"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "040313" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "NDVI"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030813" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "NIR_760"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030613" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "R_670"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030613" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "RE_730"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030813" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
  }
  return(earlyphenos)
}

getearlydata3 = function(earlyphenos, getrep="3"){
  i = 1
  for(i in i:length(earlyphenos)){
    if(names(earlyphenos[i]) == "CTD"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "040813" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "Chla"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "040813" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "HTPheight"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "031813" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "NDRE"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "030813" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "NDVI"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "031513" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "NIR_760"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "031513" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "R_670"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "031513" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
    if(names(earlyphenos[i]) == "RE_730"){
      earlyphenos[[i]] = subset(earlyphenos[[i]], date == "031513" & rep == getrep)
      earlyphenos[[i]] = droplevels(earlyphenos[[i]])
    }
  }
  return(earlyphenos)
}