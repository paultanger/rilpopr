# README #

While this repository is an archive of all code used for the following paper, it has been reorganized and simplified for users in another repository:
https://bitbucket.org/paultanger/scripts_tanger_etal_2017

Here is the associated data files:
add datadryad link here

Tanger, P., Klassen, S., et al. 2017. Field-based high throughput phenotyping rapidly identifies genomic regions controlling yield components in rice. Scientific Reports. 7:42839