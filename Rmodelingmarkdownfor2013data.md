
```r

################################################################################### model phenotypes with line and rep random ... no spatial correction

# possible models: just line lmer(pheno ~ + (1 | Line), data=data) line and
# rep lmer(pheno ~ + (1 | Line) + (1 | rep), data=data) line nested in rep
# lmer(pheno ~ + (1 | Line/rep), data=data) note that nesting lmer is not
# necessary if explicit variable names are used lmer tries to detect nesting
# and account for it in the code..  see page 30:
# http://cran.r-project.org/doc/Rnews/Rnews_2005-1.pdf or here:
# http://r.789695.n4.nabble.com/Need-help-with-lmer-model-specification-syntax-for-nested-mixed-model-td3020895.html
# lmer can determine the crossed or nested structure from the data whenever
# the data reflect the structure.  Implicitly nested factors don't reflect
# the structure of the data and rely on external information to augment the
# data given.  The computational methods used in lmer don't depend on
# whether the grouping factors for the random effects are nested or not.
# However they do require that the grouping factors are well-defined.

# ways to model multiple phenotypes:
# http://stackoverflow.com/questions/22134545/how-to-run-a-model-for-multiple-variablescolumns-in-df-with-lmer

############################# function approach linereprandommodel = function(df, varname){
############################# library(lme4) model = lmer(paste0(varname, ' ~ + (1 | Line) + (1 | rep)'),
############################# data=df) return(model) } biomass = linereprandommodel(wtht, 'biomass')
############################# biomass

############################# list approach - store all models in a list and use lapply to do things to
############################# all
```



```r
# just for report output.. source('2013FieldDatafilter_and_format.R')
source("myfunctions.R")
source("loadlibraries.R")
```

```
## Warning: package 'gridExtra' was built under R version 3.0.3
```

```r

# or load file...
setwd("C:\\Users\\paultanger\\Desktop\\Dropbox\\working folder\\Rprojects\\RILpop\\data\\")
DS2013data = read.delim("DS2013phenofinal_20140309_1523.tsv")

# rename line to Line
colnames(DS2013data)[names(DS2013data) == "line"] = "Line"
```



```r
############################# list approach - store all models in a list and use lapply to do things to
############################# all

alldata = DS2013data

# problem - we only have grain data from one rep.. remove it for now & ask
# stats people
alldata = alldata[, -9]

# make list of phenotypes
varlist = names(alldata)[6:9]

# make rep a factor
alldata$rep = as.factor(alldata$rep)

# run random line and rep models
models = lapply(varlist, function(x) {
    lmer(substitute(i ~ +(1 | Line) + (1 | rep), list(i = as.name(x))), data = alldata)
})

# name models in list..
names(models) = varlist

################################################################################### examine models

lapply(models, summary)
```

```
## $DTF
## Linear mixed model fit by REML ['lmerMod']
## Formula: DTF ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 31159 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -3.763 -0.459  0.012  0.510  3.769 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 54.74    7.40    
##  rep      (Intercept)  4.64    2.15    
##  Residual             36.12    6.01    
## Number of obs: 4449, groups: Line, 1516; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)    76.72       1.26    60.8
## 
## $DTH
## Linear mixed model fit by REML ['lmerMod']
## Formula: DTH ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 30222 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -4.065 -0.452  0.019  0.488  4.131 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 53.5     7.31    
##  rep      (Intercept) 10.4     3.22    
##  Residual             27.1     5.21    
## Number of obs: 4449, groups: Line, 1516; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)    84.35       1.87    45.1
## 
## $biomass
## Linear mixed model fit by REML ['lmerMod']
## Formula: biomass ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 45323 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -2.859 -0.593 -0.066  0.521  4.653 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 1119     33.5    
##  rep      (Intercept)  333     18.2    
##  Residual              999     31.6    
## Number of obs: 4425, groups: Line, 1516; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)    156.2       10.6    14.8
## 
## $height
## Linear mixed model fit by REML ['lmerMod']
## Formula: height ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 38396 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -4.706 -0.498 -0.006  0.509  6.698 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 910.8    30.18   
##  rep      (Intercept)  77.3     8.79   
##  Residual             109.3    10.45   
## Number of obs: 4446, groups: Line, 1516; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)   128.55       5.14      25
```

```r
lapply(models, AIC)
```

```
## $DTF
## [1] 31167
## 
## $DTH
## [1] 30230
## 
## $biomass
## [1] 45331
## 
## $height
## [1] 38404
```

```r
lapply(models, getAICc)
```

```
## $DTF
##  Line 
## 31167 
## 
## $DTH
##  Line 
## 30230 
## 
## $biomass
##  Line 
## 45331 
## 
## $height
##  Line 
## 38404
```

```r
lapply(models, BIC)
```

```
## $DTF
## [1] 31192
## 
## $DTH
## [1] 30255
## 
## $biomass
## [1] 45357
## 
## $height
## [1] 38429
```

```r

################################################################################### get stuff from models

# lapply(models, ranef) lapply(models, fixef) lapply(models, logLik)
# lapply(models, coef) lapply(models, resid) this was broken by lme4 version
# 1.1-4 :(
lapply(models, varcompsdf)
```

```
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 7.40     54.74   
##  rep      (Intercept) 2.15      4.64   
##  Residual             6.01     36.12   
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 7.31     53.5    
##  rep      (Intercept) 3.22     10.4    
##  Residual             5.21     27.1    
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 33.5     1119    
##  rep      (Intercept) 18.2      333    
##  Residual             31.6      999    
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 30.18    910.8   
##  rep      (Intercept)  8.79     77.3   
##  Residual             10.45    109.3
```

```
## $DTF
##   component variance    SD varPCT
## 1      Line   54.737 7.398 57.318
## 2       rep    4.639 2.154  4.857
## 3  Residual   36.120 6.010 37.824
## 
## $DTH
##   component variance    SD varPCT
## 1      Line    53.48 7.313  58.77
## 2       rep    10.38 3.221  11.40
## 3  Residual    27.14 5.210  29.83
## 
## $biomass
##   component variance    SD varPCT
## 1      Line   1119.2 33.45  45.67
## 2       rep    333.0 18.25  13.59
## 3  Residual    998.5 31.60  40.75
## 
## $height
##   component variance     SD varPCT
## 1      Line   910.76 30.179 82.997
## 2       rep    77.32  8.793  7.046
## 3  Residual   109.26 10.453  9.957
```

```r

# get BLUPs
modelblups = lapply(models, function(x) {
    BLUP(x, "Line")
})
# name the elements in the list with the same names as phenotypes
names(modelblups) = varlist
# write to files
blupprefix = paste("BLUPs_linereprandom_", format(Sys.time(), "%Y%m%d_%H%M"), 
    sep = "")
# sapply(names(modelblups), function (x) {write.table(modelblups[[x]],
# file=paste(blupprefix, x, 'tsv', sep='.'), sep='\t', quote=F,
# row.names=F) } ) write.table(alldata, file='fieldphenofinal.tsv',
# col.names=TRUE, row.names=FALSE, sep='\t', quote=FALSE)

# combine blups into one df.. but makes lots of extra cols...
# otherdataBLUPs = do.call(cbind, modelblups)

# or merge them
alldataBLUPs = Reduce(function(x, y) merge(x, y, by = "Line"), modelblups)

# export write.table(alldataBLUPs, file=paste(blupprefix, 'tsv', sep='.'),
# sep='\t', quote=F, row.names=F)
```


```r
################################################################################### model phenotypes with line and rep ... with spatial power correction this
################################################################################### isn't implemented in lme4...
################################################################################### http://r.789695.n4.nabble.com/lmer-and-correlation-td849697.html

# diagonal covariance structures: (i.e., enforce mutually uncorrelated
# random effects via syntax like ~ (1 | group)+ (0 + x1 | group) + (0 + x2 |
# group)) unstructured covariance matrices: (i.e. all correlations are
# estimated, ~ (1 + x1 + x2 | group)) partially diagonal, partially
# unstructured covariance (y ~ (1 + x1 | group) + (0 + x2 | group), where
# you would estimate a correlation between the random intercept and random
# slope for x1, but no correlations between the random slope for x2 and the
# random intercept and between the random slope for x2 and the random slope
# for x1).

# variance-covariance structures for the residuals?

# use nlme ??
```



```r
################################################################################### check assumptions works ok, but can't label.. lapply(models, mcp.fnc)

# plot each.. label with response name.. great! lapply(models, function(x)
# {plot(x, main=getrespname(x))})

# how to get lmer object from list: models[[1]]

par(mfrow = c(2, 2))

# get histograms of residuals
lapply(models, function(x) {
    hist(resid(x), breaks = 100, main = paste("histogram of ", getrespname(x), 
        " residuals"), xlab = "residuals")
})
```

![plot of chunk plothist](figure/plothist1.png) 

```r

# get histograms of scaled residuals
lapply(models, function(x) {
    hist(resid(x, scaled = T), breaks = 100, main = paste("histogram of ", getrespname(x), 
        " scaled residuals"), xlab = "residuals")
})
```

![plot of chunk plothist](figure/plothist2.png) 




```r
# qqplot of residuals
par(mfrow = c(2, 2))

lapply(models, function(x) {
    qqnorm(resid(x), main = paste("qqplot of ", getrespname(x), " residuals"))
    qqline(resid(x))
})
```

![plot of chunk plotstuff](figure/plotstuff1.png) 

```
## $DTF
## NULL
## 
## $DTH
## NULL
## 
## $biomass
## NULL
## 
## $height
## NULL
```

```r

# qqplot of scaled residuals
lapply(models, function(x) {
    qqnorm(resid(x, scaled = T), main = paste("qqplot of ", getrespname(x), 
        " scaled residuals"))
    qqline(resid(x, scaled = T))
})
```

![plot of chunk plotstuff](figure/plotstuff2.png) 

```
## $DTF
## NULL
## 
## $DTH
## NULL
## 
## $biomass
## NULL
## 
## $height
## NULL
```

```r

# plot abs value of res vs fitted.. to see outliers...
lapply(models, function(x) {
    plot(fitted(x), abs(resid(x)), main = paste("plot of ", getrespname(x), 
        " scaled residuals"), xlab = "abs(residuals)", ylab = "fitted")
})
```

![plot of chunk plotstuff](figure/plotstuff3.png) 

```
## $DTF
## NULL
## 
## $DTH
## NULL
## 
## $biomass
## NULL
## 
## $height
## NULL
```

```r

# observed versus fitted value in order to get this to work, need to run the
# models again and exclude outliers..  run random line and rep models
models2 = lapply(varlist, function(x) {
    lmer(substitute(i ~ +(1 | Line) + (1 | rep), list(i = as.name(x))), data = alldata, 
        na.action = na.exclude)
})

# all subsequent 'plot' calls are actually using plot from lme4.. so doesn't
# respect par..  but it uses lattice, should be able to use lattice tools
# for multiplots..
fittedplots = lapply(models2, function(x) {
    plot(x, reformulate(termlabels = "fitted(.)", response = getrespname(x)), 
        abline = c(0, 1), main = paste(getrespname(x), " obs vs fitted"), xlab = "fitted", 
        ylab = "observed")
})

# plot residuals vs predicted lapply(models, function(x) { plot(x, resid(.)
# ~ fitted(.), abline = 0, main=paste('plot of ', getrespname(x), '
# residuals'), xlab='predicted', ylab='residuals') })

# plot residuals vs predicted (by rep)
residbyrep = lapply(models, function(x) {
    plot(x, resid(., scaled = T) ~ fitted(.), abline = 0, main = paste("plot of ", 
        getrespname(x), " residuals by rep"), pch = unclass(alldata$rep), xlab = "predicted", 
        ylab = "residuals")
})

# arrange them in a 2x2 grid
do.call(grid.arrange, c(fittedplots, ncol = 2))
```

![plot of chunk plotstuff](figure/plotstuff4.png) 

```r
do.call(grid.arrange, c(residbyrep, ncol = 2))
```

![plot of chunk plotstuff](figure/plotstuff5.png) 

```r


# plot studentized residuals vs predicted lapply(models, function(x) {
# plot(x, resid(., scaled=T) ~ fitted(.), abline = 0, main=paste('plot of ',
# getrespname(x), ' scaled residuals'), xlab='predicted', ylab='scaled
# residuals') })

```



```r
# box plots of something.. doesn't really make sense..
# plot(DTHdata.random.line.rep, Line ~ resid(., scaled=TRUE)) this doesn't
# work... plot(DTHdata.random.line.rep, id=0.05, idLabels=DTHdata$Line)

# check something with residual from model...
# summary(lm(abs(resid(DTHdata.random.line.rep)) ~
# fitted(DTHdata.random.line.rep)))
# plot(jitter(fitted(DTHdata.random.line.rep)),
# resid(DTHdata.random.line.rep), xlab='Fitted', ylab='Residuals')

# get cook's distance and dfbetas blah = models[[2]] blah2 = influence(blah,
# obs = T) # or (blah, group='Line') ?  cooks.blah2 <- cooks.distance(blah2)
# ME.cook(blah2, plot=TRUE, cutoff=.17) dfbetas.blah2 <- dfbetas(blah2)
# plot(blah2, which='cook', sort=F) # combine cook's D with original df..
# when not NA..  datawcooks = cbind(cooks.blah2, alldata[!is.na(alldata[7]),
# c(1:5,7),]) # plot and label...  dotplot(Line ~ cooks.blah2,
# data=datawcooks, panel = function(x, y, ...) { panel.dotplot(x, y, ...)
# id <- cooks.blah2 > .0002 panel.text(datawcooks$cooks.blah2[id],
# datawcooks$Line[id], labels = datawcooks$Line[id], pos = 4) }) plot(blah2,
# which='dfbetas')
```



```r
################################################################################### model phenotypes with line and rep fixed ... to get LSmeans

# NOTE: These results will not match up with the same procedures on the same
# data, run in SPSS (Linear Mixed Models) or run in SAS (PROC MIXED);
# although SPSS and SAS match one another. It has been discovered that the
# descrepancy is due to different reference coding of the categorical
# variables when in SPSS and SAS compared to the 'lme4' package and 'lmer'
# function. Essentially, all the R functions used here (in this script) code
# categorical factors / variables so that the reference category is the
# category with the LOWEST numerical value (or alphabetically first letter).
# SPSS and SAS both use the opposite strategy; they code categorical factors
# / variables so that the reference category is the category with the
# HIGHEST numerical value (or alphabetically last letter). This is important
# to note because, the SPSS/SAS Mixed Effects model output produces an
# intercept term for the fixed effects which is substantially different from
# the intercept term for the fixed effects produced by the 'lme4' package;
# and of course, with different intercepts comes different predicted values
# based on the model. If interested in getting SPSS or SAS output to match
# what is produced by this script, then simply reverse code the values of
# the categorical variables when the data is imported to SPSS or SAS.
# Meaning, for instance with the class variable; any case with a value of
# 'a' would be changed to a value of 'd' and vice versa, any case with a
# value of 'c' would be changed to a value of 'b' and vice versa.

# disable scientific notation
options(scipen = 999)

# a*b means a + b + a*b a:b means a + b + a:b ....  so * and : are the
# same..  -1 or 0 means no intercept..

# get fixed effect models.. line and rep fixed..
fixedmodels = lapply(varlist, function(x) {
    lm(substitute(i ~ Line + rep, list(i = as.name(x))), data = alldata)
})

# name models in list
names(fixedmodels) = varlist

################################### updated for lsmeans version 2 see ref.grid class definition here around
################################### page 22 http://cran.r-project.org/web/packages/lsmeans/lsmeans.pdf

# get lsmeans (takes a long time) fixedmodels.lsmeans = lapply(fixedmodels,
# function(x) { lsmeans(x, ~ Line)}) convert list to dfs
listlength = length(fixedmodels.lsmeans)
```

```
## Error: object 'fixedmodels.lsmeans' not found
```

```r
# one way for (i in 1:listlength) {
# assign(paste0('lsmeansdf',getrespname(fixedmodels[[i]])),
# summary(fixedmodels.lsmeans[[i]])) } another way define empty df
lsmeansdf = as.data.frame(setNames(replicate(7, numeric(0), simplify = F), c("Line", 
    "lsmeans", "SE", "df", "lowerCI", "upperCI", "resp")))
i = 1
# this error is ok.. just ignore it... Error in make.link(misc$tran) : ‘+’
# link not recognised
for (i in 1:listlength) {
    lsmeansdf = rbind(lsmeansdf, cbind(summary(fixedmodels.lsmeans[[i]]), list(resp = getrespname(fixedmodels[[i]]))))
}
```

```
## Error: object 'listlength' not found
```

```r

# dcast it
lsmeanswide = dcast(lsmeansdf, Line ~ resp, value.var = "lsmean")
```

```
## Error: dims [product 1] do not match the length of object [0]
```

```r
# rename cols
varlistlsmeans = lapply(varlist, function(x) {
    paste(x, "_LSmeans", sep = "")
})
names(lsmeanswide) = c("Line", varlistlsmeans)
```

```
## Error: object 'lsmeanswide' not found
```

```r
################################### 

# combine blups and lsmeans
alldataestimates = merge(lsmeanswide, alldataBLUPs, by = "Line")
```

```
## Error: object 'lsmeanswide' not found
```

```r
head(alldataestimates)
```

```
## Error: error in evaluating the argument 'x' in selecting a method for function 'head': Error: object 'alldataestimates' not found
```

```r
# export
allestimatesprefix = paste("DS2013_BLUPsandLSmeans_", format(Sys.time(), "%Y%m%d_%H%M"), 
    sep = "")
# write.table(alldataestimates, file=paste(allestimatesprefix, 'tsv',
# sep='.'), sep='\t', quote=F, row.names=F)
```



```r
colnames(alldataestimates)[colnames(phealldataestimates) == "Line"] = "ID"
```

```
## Error: object 'alldataestimates' not found
```

```r
rqtlfileprefix = paste("DS2013phenosRqtl_", format(Sys.time(), "%Y%m%d_%H%M"), 
    sep = "")
# write.csv(alldataestimates, file=paste(rqtlfileprefix, 'csv',
# sep='.'),row.names=F)
```

