nicksdata = "~/Desktop/Dropbox/working folder/MI sugar release 200 lines/Tanger_2015/"
datapath = "~/Desktop/Dropbox/working folder/Rprojects/RILpop/data/"
sourcepath = "~/Desktop/Rgit/rilpopr/"

setwd(sourcepath)
source("myfunctions.R")
setwd(datapath)

# get data
setwd(nicksdata)

# load libraries
#install.packages("xlsx")
library(xlsx)
#install.packages("rJava")
#library(rJava)

# get glucose data first
file_list = list.files()
rm(Gdataraw)
# combine them
Gdataraw <- do.call("rbind",lapply(file_list,
                                  FUN=function(files){
                                    # get data
                                    df = read.xlsx(files,  stringsAsFactors=T, sheetName="ProG", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))
                                    # add col to track file name (maybe diff runs?) 
                                    df$file = substr(files, 10, 10)
                                    # return it to rbind
                                    return(df)
                                  }))

# get pentose data
rm(Pdataraw)
# combine them
Pdataraw <- do.call("rbind",lapply(file_list,
                                   FUN=function(files){
                                     # get data
                                     df = read.xlsx(files,  stringsAsFactors=T, sheetName="ProP", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))
                                     # add col to track file name (maybe diff runs?) 
                                     df$file = substr(files, 10, 10)
                                     # return it to rbind
                                     return(df)
                                   }))

# add factor for each
Gdataraw$phenotype = "glucose"
Pdataraw$phenotype = "pentose"

# combine them
dataraw = rbind(Gdataraw,Pdataraw)

# remove blank rows
dataraw = dataraw[rowSums(is.na(dataraw))==0,]

# filter for just my barcodes
dataraw = dataraw[grep("^TANGER",dataraw$Input),]

# drop unused levels
dataraw = droplevels(dataraw)

# merge with our IDs
setwd(nicksdata)
setwd("../")
BarcodeToLine = read.csv("WithBarcodeNeedSugarReleasePheno_FINAL.csv", colClasses = c(rep("character", 2)))
BarcodeToLine = BarcodeToLine[,c(1:4)]
setwd(datapath)
PlotToLine = read.csv("F7_Plot_and_line.csv", colClasses=c("numeric", "factor"))
# ControlBarcodes = read.csv("all_controlbarcode_IDs.txt")

# change col names to make merge easier
colnames(dataraw)[colnames(dataraw)=="Input"] = "barcode"
colnames(dataraw)[colnames(dataraw)=="Output"] = "well_rep"

# merge line and plot
BarcodeToLine = merge(BarcodeToLine[,c(1,3,4)], PlotToLine, all.x=T)

# do the merge, first with the full list
dataID = merge(dataraw, BarcodeToLine[,c(2:4)], by="barcode", all.x=T)

setwd(datapath)
filename = addStampToFilename("200linesrawsugardatatocheck", "tsv")
# export to check 
#write.table(dataID, file=filename, col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)

dataID$file = as.factor(dataID$file)
dataID$phenotype = as.factor(dataID$phenotype)
dataID$rep = as.factor(dataID$rep)
dataID = droplevels(dataID)

#combine based on barcode and well reps.. for the 4 abs readings..
library("reshape2")

dataIDlong = melt(dataID, measure.vars=c("X1st", "X2nd", "X3rd", "X4th"), variable_name="well_rep")

# check it
head(dataID)
dataIDlong[(dataIDlong$well_rep==8015635327 & dataIDlong$phenotype == "pentose"),]
head(dataIDlong)

# get means of 4 assay reps
# dataIDlongmeans = aggregate(value ~ phenotype + file + rep + barcode + Line + well_rep, data=dataIDlong, mean)
head(dataIDlongmeans)
# another way with variation of assay reps
summary.stats <- aggregate(value ~ phenotype + file + rep + barcode + Line + well_rep, data=dataIDlong, FUN=function(x) c(length(x), mean(x), sd(x), sd(x)/sqrt(length(x)), sd(x)/mean(x)))
summary.stats <- cbind(summary.stats[,1:6], as.data.frame(summary.stats[,7]))
names(summary.stats) <- c("phenotype", "file", "rep", "barcode", "Line", "well_rep", "n_assay_reps", "mean", "SD", "SE", "CV")

# write to check it..
filename = addStampToFilename("200linesassaymeanssugardatatocheck", "tsv")
#write.table(summary.stats, file=filename, col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)

# discard when CV > 50%
summary.stats2 = summary.stats[summary.stats$CV < 0.50,]

head(summary.stats2)
# do it again, but this time get means of well reps (3 tech reps)
summary.stats3 <- aggregate(mean ~ phenotype + file + rep + barcode + Line, data=summary.stats2[,c(1:6,8)], FUN=function(x) c(length(x), mean(x), sd(x), sd(x)/sqrt(length(x)), sd(x)/mean(x)))
summary.stats3 <- cbind(summary.stats3[,1:5], as.data.frame(summary.stats3[,6]))
names(summary.stats3) <- c("phenotype", "file", "rep", "barcode", "Line", "n_assay_reps", "mean", "SD", "SE", "CV")

# write to check it..
filename = addStampToFilename("200lineswellmeanssugardatatocheck", "tsv")
#write.table(summary.stats3, file=filename, col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)

# discard when CV > 50%
summary.stats3 = summary.stats3[summary.stats3$CV < 0.40,]

head(summary.stats3)
# change to percents (easier to read)
summary.stats3$mean = summary.stats3$mean*100

filename = addStampToFilename("200linesSugarMeans", "tsv")
#write.table(summary.stats3, file=filename, col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)
filename = addStampToFilename("200linesSugarMeans", "Robject")
save(summary.stats3, file=filename)
