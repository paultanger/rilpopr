###################################################################################
# load stuff ... careful what working dir you set... then load libraries..
###################################################################################

# load the custom functions in myfunctions.R file..
# do this before setting wd..
source("myfunctions.R")
source("loadlibraries.R")

# for windows
setwd("C:\\Users\\paultanger\\Desktop\\Dropbox\\working folder\\Rprojects\\RILpop\\data\\")
# for server
setwd("~/Dropbox/Rprojects/RILpop/data/")
# for mac
setwd("~/Desktop/Dropbox/working\ folder/Rprojects/RILpop/data/")

# load data from dropbox
load(".RData")

# save data
save.image()
