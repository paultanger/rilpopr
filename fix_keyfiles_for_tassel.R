# take the definitive AZ screwed up IDs and replace with proper line IDs
# this is in the key files in order to run tassel genotyping

setwd("C:\\Users\\paultanger\\Desktop\\Dropbox\\working folder\\F7\\GENOTYPING\\feb2014AZdata")

# get the definitive file
correctIDs = read.delim("definitive_AZ_ID_to_correct_lineID_file.tsv", stringsAsFactors=F)

# get the keyfile.. not sure why they have two for R1 and R2.. it is the same thing??
keyfile = read.delim("key_file_Run127_R1.txt", header=F, stringsAsFactors=F)
# rename
colnames(keyfile) = c("flowcell", "lane", "barcode", "AZ_ID", "plate", "row", "col")
# sort
keyfile = keyfile[order(keyfile$AZ_ID),]
correctIDs = correctIDs[order(correctIDs$final_line_ID),]

# merge together
# keyfilefinal = merge(keyfile, correctIDs, by="AZ_ID")
# why are some missing?
# which ones?
# keyfilefinal2 = merge(keyfile, correctIDs, by="AZ_ID", all.y=T)
# keyfilefinal2[is.na(keyfilefinal2$barcode),]
# keyfilefinal3 = merge(keyfile, correctIDs, by="AZ_ID", all.x=T)
# keyfilefinal3[is.na(keyfilefinal3$final_line_ID),]

# these are in their list but not mine...
# AZ_ID   machine lane     barcode             plate row col Barcode final_line_ID
# 1   Aswina2 H0PL2ADXX    1 ATACGCACAAG IR64xAswinaPlate1   F   9    <NA>          <NA>
# 2    IR64-2 H0PL2ADXX    1 AGGACCTAGGC IR64xAswinaPlate1   F   4    <NA>          <NA>
# 4     L1039 H0PL2ADXX    1 GGTGTCAAGCT IR64xAswinaPlate2   G   1    <NA>          <NA>
# 14    L1162 H0PL2ADXX    1 AACGTCAAGCT IR64xAswinaPlate1   B   3    <NA>          <NA>
# 111    L685 H0PL2ADXX    1 GCCATCGATCT IR64xAswinaPlate2   E  10    <NA>          <NA>

# the parents I can fix, and maybe they just did one 685..
# I think these are all typos that should have been fixed...
# confirmed by jayson on 7/22/13
# so 1039 should be 1139
# so 1162 should be 1062

# these are in my list but not their's.. why not sequenced?
#      AZ_ID machine lane barcode plate  row col     Barcode final_line_ID
# 278  L1502    <NA>   NA    <NA>  <NA> <NA>  NA AACACCGGACG          1502
# 279  L1511    <NA>   NA    <NA>  <NA> <NA>  NA CCAACCGGACG          1511
# 280  L1523    <NA>   NA    <NA>  <NA> <NA>  NA GGTGTCGGACG          1523
# 281  L1526    <NA>   NA    <NA>  <NA> <NA>  NA TTGCGCGGACG          1526
# 282  L1528    <NA>   NA    <NA>  <NA> <NA>  NA AACTACGGACG          1528
# 283  L1545    <NA>   NA    <NA>  <NA> <NA>  NA GTCCACGGACG          1545
# 284  L1565    <NA>   NA    <NA>  <NA> <NA>  NA GTTGACGGACG          1565
# 285  L1577    <NA>   NA    <NA>  <NA> <NA>  NA ACGTCCGGACG          1577
# 286  L1605    <NA>   NA    <NA>  <NA> <NA>  NA AACGTCGGACG          1605
# 287   L247    <NA>   NA    <NA>  <NA> <NA>  NA AGGACCCTCGT           247
# 288   L261    <NA>   NA    <NA>  <NA> <NA>  NA ATACGCCTCGT           261
# 289   L274    <NA>   NA    <NA>  <NA> <NA>  NA CACTGCCTCGT           274
# 290   L287    <NA>   NA    <NA>  <NA> <NA>  NA CTGAGCCTCGT           287
# 291   L288    <NA>   NA    <NA>  <NA> <NA>  NA GAACGCCTCGT           288
# 292   L291    <NA>   NA    <NA>  <NA> <NA>  NA GCCATCCTCGT           291
# 293   L318    <NA>   NA    <NA>  <NA> <NA>  NA TACAGCCTCGT           318
# 294   L322    <NA>   NA    <NA>  <NA> <NA>  NA CGTGACCTCGT           322
# 295   L323    <NA>   NA    <NA>  <NA> <NA>  NA GCGTACCTCGT           323
# 296   L328    <NA>   NA    <NA>  <NA> <NA>  NA TATGTCCTCGT           328
# 297   L340    <NA>   NA    <NA>  <NA> <NA>  NA TACTTCCTCGT           340
# 298   L369    <NA>   NA    <NA>  <NA> <NA>  NA CGTTGCCTCGT           369
# 299  L685a    <NA>   NA    <NA>  <NA> <NA>  NA GTCCACGGTCA          685a  ## ok, was done
# 300  L685b    <NA>   NA    <NA>  <NA> <NA>  NA GCCATCGATCT          685b  ## ok, was done
# 301    L78    <NA>   NA    <NA>  <NA> <NA>  NA CCAACCCTCGT            78
# 302 P28859    <NA>   NA    <NA>  <NA> <NA>  NA GTTGACCTCGT          1054
# 303 P28864    <NA>   NA    <NA>  <NA> <NA>  NA TGACGCCTCGT          1063

# so update the correctIDs with what I mentioned above...
correctIDs$AZ_ID [correctIDs$AZ_ID == "L685b" ] = "L685"
correctIDs$AZ_ID [correctIDs$AZ_ID  == "Aswina" ] = "Aswina2"
correctIDs$AZ_ID [correctIDs$AZ_ID  == "IR64" ] = "IR64-2"
keyfile$AZ_ID[keyfile$AZ_ID == "L1039" ] = "L1139"
keyfile$AZ_ID[keyfile$AZ_ID == "L1162" ] = "L1062"

# and try it again
keyfilefinal = merge(keyfile, correctIDs, by="AZ_ID")

# ok, this looks better.. still not sure why a bunch aren't sequenced this time...
# remove unneeded cols..
head(keyfilefinal)
keyfinal = keyfilefinal[,c(2:4,9,5:7)]
head(keyfinal)

# also the flowcell is wrong, so fix it:
keyfinal$flowcell[keyfinal$flowcell == "H0PL2ADXX" ] = "H8ECVADXX"

# actually we can just combine the two lanes together in one keyfile
run128 = read.delim("key_file_Run128_R1.txt", header=F, stringsAsFactors=F, colClasses = c(rep("character", 7)))
colnames(run128) = c("flowcell", "lane", "barcode", "final_line_ID", "plate", "row", "col")
run128$final_line_ID[run128$final_line_ID == "Aswina2" ] = "Aswina"
run128$final_line_ID[run128$final_line_ID == "IR64-2" ] = "IR64"

final = rbind(keyfinal, run128)

# write to file
prefix = paste("feb2014AZdatakeyfile_", format(Sys.time(),"%Y%m%d_%H%M"), sep="")
#write.table(final, file=paste(prefix, "tsv", sep="."), sep="\t", quote=F, row.names=F, col.names=F)

#########
# lets take these, and add the keyfiles for the Oct & Nov AZ data... and run all together?
# not sure why we have different numbers of samples in each run...
# also, edit the definitive list to remove a or b - tassel keeps track of this

# load definitive key list
correctIDs = read.delim("definitive_AZ_ID_to_correct_lineID_file.tsv", stringsAsFactors=F)

# load feb key list
febkey = read.delim("feb2014AZdatakeyfile_20140318_2308.tsv", stringsAsFactors=F, header=F)
colnames(febkey) = c("flowcell", "lane", "barcode", "final_line_ID", "plate", "row", "col")

# R1 and R2 are different.. not sure why..
octkey = read.delim("OctIR64_aswina_R1.keyfile.txt", header=T, stringsAsFactors=F)
colnames(octkey) = c("flowcell", "lane", "barcode", "AZ_ID", "plate", "row", "col")

# nov data
novkeylane1 = read.delim("Novlane1_R1.keyfile.txt", header=F, stringsAsFactors=F)
novkeylane2 = read.delim("Novlane2_R1.keyfile.txt", header=F, stringsAsFactors=F)

colnames(novkeylane1) = c("flowcell", "lane", "barcode", "AZ_ID", "plate", "row", "col")
colnames(novkeylane2) = c("flowcell", "lane", "barcode", "AZ_ID", "plate", "row", "col")

octkey = octkey[order(octkey$AZ_ID),]
novkeylane1 = novkeylane1[order(novkeylane1$AZ_ID),]
novkeylane2 = novkeylane2[order(novkeylane2$AZ_ID),]

head(octkey)
head(novkeylane1)
head(novkeylane2)

# try to merge with correct IDs
octfinal = merge(octkey, correctIDs, by="AZ_ID")
# ok looks good.. remove extra cols
octfinal = octfinal[,c(2:4,9,5:7)]

# now try with nov data
nov1final = merge(novkeylane1, correctIDs, by="AZ_ID")
nov2final = merge(novkeylane2, correctIDs, by="AZ_ID")

nov1final = nov1final[,c(2:4,9,5:7)]
nov2final = nov2final[,c(2:4,9,5:7)]

head(octfinal)
head(nov1final)
head(nov2final)
head(febkey)

# merge them all together
octnovfeb = rbind(octfinal,nov1final,nov2final,febkey)

# write to file
prefix = paste("ALLAZdatakeyfile_", format(Sys.time(),"%Y%m%d_%H%M"), sep="")
#write.table(octnovfeb, file=paste(prefix, "tsv", sep="."), sep="\t", quote=F, row.names=F, col.names=F)
