# get functions
datapath = "~/Desktop/rice_snps_v2/snps_cov5/"
sourcepath = "~/Desktop/Rgit/rilpopr/"


setwd(sourcepath)
source("myfunctions.R")
setwd(datapath)

# read in each lane hapmap data
lane4 = read.delim("4snps_cov5_20150615173543.hapmap", sep="\t", header=T)
lane5 = read.delim("5snps_cov5_20150615174103.hapmap", sep="\t", header=T)
lane6 = read.delim("6snps_cov5_20150615174111.hapmap", sep="\t", header=T)
lane7 = read.delim("7snps_cov5_20150615174119.hapmap", sep="\t", header=T)

# make list of data
data = list(lane4, lane5, lane6, lane7)
names(data) = c("lane4", "lane5", "lane6", "lane7")

# make into matrices
datamatrices = lapply(data, function(x) {return(as.matrix(x))})
datamatrices = lapply(data, as.matrix)
test = datamatrices[[1]]

# remove extra stuff for later
headers = lapply(data, function(x) {
  return(x[,c(1:11)])
})
# make one big df
headerdf <- do.call("rbind", headers)
# just keep unique.. there are repeats obvy
headerfinal = subset(headerdf,!duplicated(headerdf$rs.))

final = lapply(data, function(x) {
  return(x[,c(1,12:ncol(x))])
})
test = head(final[[1]])
test[c(1:5),c(1:10)]

# what if we just merge them like this
merged = merge(final[[1]], final[[2]], by="rs.", all=T)
merged = merge(merged, final[[3]], by="rs.", all=T)
merged = merge(merged, final[[4]], by="rs.", all=T, suffixes=c("e", "f"))

# another way
merged <- final[[1]]
for ( .df in final ) {
  merged <-merge(merged, .df, by="rs.", all=T, suffixes=c("e", "f"))
}
# doesn't work..Error in match.names(clabs, names(xi)) : names do not match previous names

# merge with header data
finalmerge = merge(headerfinal, merged, by="rs.", all=T)

# sort it
finalmerge = finalmerge[with(finalmerge, order(chrom, pos)),]

# check duplicates
individuals = as.data.frame(colnames(finalmerge[,c(12:ncol(finalmerge))]))
colnames(individuals) = "lines"
filename = addStampToFilename("names", "tsv")
write.table(individuals, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

# remove X from names
individuals$fixed = gsub('^X', "", individuals$lines)

# write it.. NA becomes N
filename = addStampToFilename("mergedlanes", "hapmap")
write.table(finalmerge, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE, na="N")
# 
# # transpose them so we can merge by sites
# datat = lapply(data, t)
# test = head(datat[[1]])
# 
# # fix colnames
# datat = lapply(datat, function(x) {
#   colnames(x) = x[1,]
#   return (x)
#   })
# test = head(datat[[1]])
# test[c(1:5),c(1:10)]
# 
# # remove extra stuff for later
# headers = lapply(datat, function(x) {
#   return(x[c(1:11),])
# })
# 
# final = lapply(datat, function(x) {
#   return(x[c(12:nrow(x)),])
# })
# test = head(final[[1]])
# test[c(1:5),c(1:10)]
# 
# # merge and keep all sites
# # Reduce(function(x, y) merge(x, y, by="Species"), list(iris, iris, iris))
# # mergefunction <- function(x,y){merge(x, y, all=T)}
# # merged = lapply(final, mergefunction)
# merged = merge(final[[1]], final[[2]], all=T)
# 
# 
