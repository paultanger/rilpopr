# get data
setwd("C:\\Users\\paultanger\\Desktop\\Dropbox\\working folder\\MI sugar release 100 lines\\results\\")
setwd("~/Desktop/Dropbox/working folder/MI sugar release 100 lines/results")

# load libraries
#install.packages("xlsx")
library(xlsx)
#install.packages("rJava")
#library(rJava)

# setup filenames
first   <- "Colorodo1.xls"
second  <- "Colorodo2.xls"
third   <- "Colorodo3.xls"
fourth  <- "Colorodo4.xls"
fifth   <- "Colorodo5.xls"
 
# disable stupid factors whenever I have strings..
# import the files from excel

# this only imports the first 6 columns..
# first glucose
firstG  <-read.xlsx(first,  stringsAsFactors=T,sheetName="ProG", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))
secondG <-read.xlsx(second, stringsAsFactors=T,sheetName="ProG", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))
thirdG  <-read.xlsx(third,  stringsAsFactors=T,sheetName="ProG", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))
fourthG <-read.xlsx(fourth, stringsAsFactors=T,sheetName="ProG", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))
fifthG  <-read.xlsx(fifth,  stringsAsFactors=T,sheetName="ProG", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))

# keep track of which day (file)
firstG$day  = 1
secondG$day = 2
thirdG$day  = 3
fourthG$day = 4
fifthG$day  = 5

# then pentose
firstP  <-read.xlsx(first,  stringsAsFactors=T,sheetName="ProP", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))
secondP <-read.xlsx(second, stringsAsFactors=T,sheetName="ProP", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))
thirdP  <-read.xlsx(third,  stringsAsFactors=T,sheetName="ProP", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))
fourthP <-read.xlsx(fourth, stringsAsFactors=T,sheetName="ProP", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))
fifthP  <-read.xlsx(fifth,  stringsAsFactors=T,sheetName="ProP", colIndex=1:6, colClasses=c(rep("character", 2), rep("numeric", 4)))

# combine glucose
glucose = rbind(firstG, secondG, thirdG, fourthG, fifthG)

# combine pentose
pentose = rbind(firstP, secondP, thirdP, fourthP, fifthP)

# remove blank rows
glucose = glucose[rowSums(is.na(glucose))==0,]
pentose = pentose[rowSums(is.na(pentose))==0,]

# filter for just my barcodes
glucosef = glucose[grep("^I",glucose$Input),]
pentosef = pentose[grep("^I",pentose$Input),]

# drop unused levels
glucosef = droplevels(glucosef)
pentosef = droplevels(pentosef)

# check data
# head(glucosef)
# tail(glucosef)
# head(pentosef)
# tail(pentosef)
# str(glucosef)
# str(pentosef)

#write.table(glucosef, file="glucose.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)
#write.table(pentosef, file="pentose.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)

# merge with our IDs
BarcodeToLine = read.csv("BarcodesAndPlotIDsALL.txt", colClasses = c(rep(NA, 4), rep("character", 1), rep("numeric", 4)), stringsAsFactors=T)
LeafBarcodes = read.csv("all_leafbarcode_IDs.txt")
ControlBarcodes = read.csv("all_controlbarcode_IDs.txt")

# str(BarcodeToLine)
# str(LeafBarcodes)
# str(ControlBarcodes)

# change col names to make merge easier
colnames(glucosef)[colnames(glucosef)=="Input"] = "barcode"
colnames(glucosef)[colnames(glucosef)=="Output"] = "well_rep"

colnames(pentosef)[colnames(pentosef)=="Input"] = "barcode"
colnames(pentosef)[colnames(pentosef)=="Output"] = "well_rep"

# do the merge, first with the full list
glucoseID = merge(glucosef, BarcodeToLine, by="barcode", all.x=T)
pentoseID = merge(pentosef, BarcodeToLine, by="barcode", all.x=T)

# then with the leaf and control IDs
# not sure this is necessary... maybe do this later..

# export to check in access
#write.table(glucoseID, file="glucoseID.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)
#write.table(pentoseID, file="pentoseID.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)

# change things to factors 
# str(glucoseID)
# str(pentoseID)

glucoseID$PlotID = as.factor(glucoseID$PlotID)
glucoseID$ID = as.factor(glucoseID$ID)
glucoseID$Line = as.factor(glucoseID$Line)
glucoseID$rep = as.factor(glucoseID$rep)
glucoseID$block = as.factor(glucoseID$block)

pentoseID$PlotID = as.factor(pentoseID$PlotID)
pentoseID$ID = as.factor(pentoseID$ID)
pentoseID$Line = as.factor(pentoseID$Line)
pentoseID$rep = as.factor(pentoseID$rep)
pentoseID$block = as.factor(pentoseID$block)

glucoseID = droplevels(glucoseID)
pentoseID = droplevels(pentoseID)

# str(glucoseID)
# str(pentoseID)

#combine based on barcode and well reps.. for the 4 abs readings..
library("reshape2")

glucoseL = melt(glucoseID, measure.vars=c("X1st", "X2nd", "X3rd", "X4th"), variable_name="assay_rep")
pentoseL = melt(pentoseID, measure.vars=c("X1st", "X2nd", "X3rd", "X4th"), variable_name="assay_rep")

# str(glucoseL)
# str(pentoseL)

#rename some cols
colnames(glucoseL)[colnames(glucoseL)=="value"] = "glucose"
colnames(pentoseL)[colnames(pentoseL)=="value"] = "pentose"

# write to check it..
#write.table(glucoseL, file="glucoseL.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)

# get means of 4 assay reps
glucoseM = aggregate(glucose ~ barcode + well_rep, data=glucoseL, mean)
pentoseM = aggregate(pentose ~ barcode + well_rep, data=pentoseL, mean)

# write to check it..
#write.table(glucoseM, file="glucoseMEANS.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)

# recombine with sample info
glucoseMID = merge(glucoseM, BarcodeToLine, by="barcode")
pentoseMID = merge(pentoseM, BarcodeToLine, by="barcode")

# write to check it..
#write.table(glucoseMID, file="glucoseMID.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)
#write.table(pentoseMID, file="pentoseMID.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)

# combine glucose and pentose.. there is one pentose without a matching glucose...
# first get rid of duplicate columns we don't need... or could have used glucoseM from above..
glucoseMIDformerge = glucoseMID[1:3]
togetherMID = merge(pentoseMID, glucoseMIDformerge, by=c("barcode", "well_rep"), all=T)

# check for NA
# subset(togetherMID, is.na(glucose))
# subset(togetherMID, is.na(pentose))

# write to check it..
#write.table(togetherMID, file="together.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)

# if first 10 letters are "IR64 block" then it is IR64..
togetherMID$Line[substr(togetherMID$BarcodeIDafterplotcorrections,1,10) == "IR64 block"] = "IR64-21"

# if first 12 letters are "Aswina block" then it is Aswina..
togetherMID$Line[substr(togetherMID$BarcodeIDafterplotcorrections,1,12) == "Aswina block"] = "Aswina"

# if last 5 letters are "rep 1" then rep 1.. etc..
togetherMID$rep[substr(togetherMID$BarcodeIDafterplotcorrections, 
                       nchar(as.character(togetherMID$BarcodeIDafterplotcorrections))-4, 
                       nchar(as.character(togetherMID$BarcodeIDafterplotcorrections))    ) == "rep 1"] = 1

togetherMID$rep[substr(togetherMID$BarcodeIDafterplotcorrections, 
                       nchar(as.character(togetherMID$BarcodeIDafterplotcorrections))-4, 
                       nchar(as.character(togetherMID$BarcodeIDafterplotcorrections))    ) == "rep 2"] = 2

togetherMID$rep[substr(togetherMID$BarcodeIDafterplotcorrections, 
                       nchar(as.character(togetherMID$BarcodeIDafterplotcorrections))-4, 
                       nchar(as.character(togetherMID$BarcodeIDafterplotcorrections))    ) == "rep 3"] = 3

# extract controls to compare with old data
controls = togetherMID[togetherMID$Line == "",]

# remove controls
togetherNoControls = togetherMID[togetherMID$Line != "",]

# write to check it..
#write.table(controls, file="controls.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)
#write.table(togetherNoControls, file="togetherNoControls.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)

# change to percents (easier to read)
togetherNoControls$glucose = togetherNoControls$glucose*100
togetherNoControls$pentose = togetherNoControls$pentose*100

togetherNoControls$PlotID = as.factor(togetherNoControls$PlotID)
togetherNoControls$Line = as.factor(togetherNoControls$Line)
togetherNoControls$rep = as.factor(togetherNoControls$rep)

togetherNoControls = droplevels(togetherNoControls)

#write.table(togetherNoControls, file="togetherForModelsWithParents.tsv", col.names=TRUE, row.names=FALSE, sep="\t", quote=FALSE)
