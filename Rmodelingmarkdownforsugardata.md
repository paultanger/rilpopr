
```r

################################################################################### model phenotypes with line and rep random ... no spatial correction

# possible models: just line lmer(pheno ~ + (1 | Line), data=data) line and
# rep lmer(pheno ~ + (1 | Line) + (1 | rep), data=data) line nested in rep
# lmer(pheno ~ + (1 | Line/rep), data=data) note that nesting lmer is not
# necessary if explicit variable names are used lmer tries to detect nesting
# and account for it in the code..  see page 30:
# http://cran.r-project.org/doc/Rnews/Rnews_2005-1.pdf or here:
# http://r.789695.n4.nabble.com/Need-help-with-lmer-model-specification-syntax-for-nested-mixed-model-td3020895.html
# lmer can determine the crossed or nested structure from the data whenever
# the data reflect the structure.  Implicitly nested factors don't reflect
# the structure of the data and rely on external information to augment the
# data given.  The computational methods used in lmer don't depend on
# whether the grouping factors for the random effects are nested or not.
# However they do require that the grouping factors are well-defined.

# ways to model multiple phenotypes:
# http://stackoverflow.com/questions/22134545/how-to-run-a-model-for-multiple-variablescolumns-in-df-with-lmer

############################# function approach linereprandommodel = function(df, varname){
############################# library(lme4) model = lmer(paste0(varname, ' ~ + (1 | Line) + (1 | rep)'),
############################# data=df) return(model) } biomass = linereprandommodel(wtht, 'biomass')
############################# biomass

############################# list approach - store all models in a list and use lapply to do things to
############################# all
```



```r
# just for report output.. source('merge_nicksSamplesv4.R')
source("myfunctions.R")
source("loadlibraries.R")
```

```
## Warning: package 'gridExtra' was built under R version 3.0.3
```

```r

# or load file...
setwd("C:\\Users\\paultanger\\Desktop\\Dropbox\\working folder\\Rprojects\\RILpop\\data\\")
togetherNoControls = read.table("togetherForModels.tsv", sep = "\t", header = T)
togetherNoControls$well_rep = as.factor(togetherNoControls$well_rep)
togetherNoControls$PlotID = as.factor(togetherNoControls$PlotID)
togetherNoControls$Line = as.factor(togetherNoControls$Line)
togetherNoControls$rep = as.factor(togetherNoControls$rep)
togetherNoControls$row = as.factor(togetherNoControls$row)
togetherNoControls$col = as.factor(togetherNoControls$col)


# get means by line and rep together.means = aggregate(cbind(glucose,
# pentose) ~ Line + rep, togetherNoControls, mean) for now, I run plot level
# stuff..
together.means = aggregate(cbind(glucose, pentose) ~ Line + PlotID + rep + row + 
    col, togetherNoControls, mean)
```



```r
############################# list approach - store all models in a list and use lapply to do things to
############################# all

alldata = together.means

# make list of phenotypes
varlist = names(alldata)[6:7]

# make rep a factor
alldata$rep = as.factor(alldata$rep)

# run random line and rep models
models = lapply(varlist, function(x) {
    lmer(substitute(i ~ +(1 | Line) + (1 | rep), list(i = as.name(x))), data = alldata)
})

# name models in list..
names(models) = varlist

################################################################################### examine models

lapply(models, summary)
```

```
## $glucose
## Linear mixed model fit by REML ['lmerMod']
## Formula: glucose ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 986.6 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -3.727 -0.632 -0.030  0.508  4.648 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 0.367    0.606   
##  rep      (Intercept) 0.131    0.362   
##  Residual             1.070    1.034   
## Number of obs: 312, groups: Line, 101; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)   12.805      0.225    56.8
## 
## $pentose
## Linear mixed model fit by REML ['lmerMod']
## Formula: pentose ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 500.5 
## 
## Scaled residuals: 
##     Min      1Q  Median      3Q     Max 
## -2.3966 -0.5826 -0.0953  0.5724  3.0719 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 0.1315   0.363   
##  rep      (Intercept) 0.0412   0.203   
##  Residual             0.1968   0.444   
## Number of obs: 312, groups: Line, 101; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)    2.935      0.125    23.4
```

```r
lapply(models, AIC)
```

```
## $glucose
## [1] 994.6
## 
## $pentose
## [1] 508.5
```

```r
lapply(models, getAICc)
```

```
## $glucose
## Line 
##  995 
## 
## $pentose
##  Line 
## 508.9
```

```r
lapply(models, BIC)
```

```
## $glucose
## [1] 1010
## 
## $pentose
## [1] 523.5
```

```r

################################################################################### get stuff from models

# lapply(models, ranef) lapply(models, fixef) lapply(models, logLik)
# lapply(models, coef) lapply(models, resid) this was broken by lme4 version
# 1.1-4 :(
lapply(models, varcompsdf)
```

```
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 0.606    0.367   
##  rep      (Intercept) 0.362    0.131   
##  Residual             1.034    1.070   
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 0.363    0.1315  
##  rep      (Intercept) 0.203    0.0412  
##  Residual             0.444    0.1968
```

```
## $glucose
##   component variance     SD varPCT
## 1      Line   0.3671 0.6059 23.406
## 2       rep   0.1313 0.3623  8.369
## 3  Residual   1.0700 1.0344 68.225
## 
## $pentose
##   component variance     SD varPCT
## 1      Line  0.13150 0.3626  35.59
## 2       rep  0.04121 0.2030  11.15
## 3  Residual  0.19676 0.4436  53.25
```

```r

# get BLUPs
modelblups = lapply(models, function(x) {
    BLUP(x, "Line")
})
# name the elements in the list with the same names as phenotypes
names(modelblups) = varlist
# write to files
blupprefix = paste("BLUPs_linereprandom_", format(Sys.time(), "%Y%m%d_%H%M"), 
    sep = "")
# sapply(names(modelblups), function (x) {write.table(modelblups[[x]],
# file=paste(blupprefix, x, 'tsv', sep='.'), sep='\t', quote=F,
# row.names=F) } ) write.table(alldata, file='fieldphenofinal.tsv',
# col.names=TRUE, row.names=FALSE, sep='\t', quote=FALSE)

# combine blups into one df.. but makes lots of extra cols...
# otherdataBLUPs = do.call(cbind, modelblups)

# or merge them
alldataBLUPs = Reduce(function(x, y) merge(x, y, by = "Line"), modelblups)

# export write.table(alldataBLUPs, file=paste(blupprefix, 'tsv', sep='.'),
# sep='\t', quote=F, row.names=F)
```


```r
################################################################################### model phenotypes with line and rep ... with spatial power correction this
################################################################################### isn't implemented in lme4...
################################################################################### http://r.789695.n4.nabble.com/lmer-and-correlation-td849697.html

# diagonal covariance structures: (i.e., enforce mutually uncorrelated
# random effects via syntax like ~ (1 | group)+ (0 + x1 | group) + (0 + x2 |
# group)) unstructured covariance matrices: (i.e. all correlations are
# estimated, ~ (1 + x1 + x2 | group)) partially diagonal, partially
# unstructured covariance (y ~ (1 + x1 | group) + (0 + x2 | group), where
# you would estimate a correlation between the random intercept and random
# slope for x1, but no correlations between the random slope for x2 and the
# random intercept and between the random slope for x2 and the random slope
# for x1).

# variance-covariance structures for the residuals?

# use nlme ??
```



```r
################################################################################### check assumptions works ok, but can't label.. lapply(models, mcp.fnc)

# plot each.. label with response name.. great! lapply(models, function(x)
# {plot(x, main=getrespname(x))})

# how to get lmer object from list: models[[1]]

# get histograms of residuals define rows and columns to put together
# graphs... so first 4 together... first number is row..
par(mfrow = c(2, 2))
lapply(models, function(x) {
    hist(resid(x), breaks = 100, main = paste("histogram of ", getrespname(x), 
        " residuals"), xlab = "residuals")
})

# get histograms of scaled residuals
lapply(models, function(x) {
    hist(resid(x, scaled = T), breaks = 100, main = paste("histogram of ", getrespname(x), 
        " scaled residuals"), xlab = "residuals")
})
```

![plot of chunk plothist](figure/plothist.png) 




```r
# qqplot of residuals
par(mfrow = c(2, 2))
lapply(models, function(x) {
    qqnorm(resid(x), main = paste("qqplot of ", getrespname(x), " residuals"))
    qqline(resid(x))
})
```

```
## $glucose
## NULL
## 
## $pentose
## NULL
```

```r

# qqplot of scaled residuals
lapply(models, function(x) {
    qqnorm(resid(x, scaled = T), main = paste("qqplot of ", getrespname(x), 
        " scaled residuals"))
    qqline(resid(x, scaled = T))
})
```

![plot of chunk plotstuff](figure/plotstuff.png) 

```
## $glucose
## NULL
## 
## $pentose
## NULL
```

```r
# plot abs value of res vs fitted.. to see outliers...  just use half the
# space.. don't stretch them..  you list 4 values, but only use the first
# two... layout.show(2) this doesn't seem to work with knitr...
layout(matrix(c(1, 2, 3, 4), 2, 2, byrow = T))
lapply(models, function(x) {
    plot(fitted(x), abs(resid(x)), main = paste("plot of ", getrespname(x), 
        " residuals"), xlab = "abs(residuals)", ylab = "fitted")
})
```

```
## $glucose
## NULL
## 
## $pentose
## NULL
```

![plot of chunk newsec](figure/newsec.png) 


```r
# observed versus fitted value in order to get this to work, need to run the
# models again and exclude outliers..  run random line and rep models
models2 = lapply(varlist, function(x) {
    lmer(substitute(i ~ +(1 | Line) + (1 | rep), list(i = as.name(x))), data = alldata, 
        na.action = na.exclude)
})

# all subsequent 'plot' calls are actually using plot from lme4.. so doesn't
# respect par..  but it uses lattice, should be able to use lattice tools
# for multiplots..

fittedplots = lapply(models2, function(x) {
    plot(x, reformulate(termlabels = "fitted(.)", response = getrespname(x)), 
        abline = c(0, 1), main = paste(getrespname(x), " obs vs fitted"), xlab = "fitted", 
        ylab = "observed")
})


# plot residuals vs predicted lapply(models, function(x) { plot(x, resid(.)
# ~ fitted(.), abline = 0, main=paste('plot of ', getrespname(x), '
# residuals'), xlab='predicted', ylab='residuals') })

# plot residuals vs predicted (by rep)
residbyrep = lapply(models, function(x) {
    plot(x, resid(., scaled = T) ~ fitted(.), abline = 0, main = paste("plot of ", 
        getrespname(x), " residuals by rep"), pch = unclass(alldata$rep), xlab = "predicted", 
        ylab = "residuals")
})

# combine lists
plots = c(fittedplots, residbyrep)

# arrange them in a 2x2 grid
do.call(grid.arrange, c(plots, nrow = 2))
```

![plot of chunk another](figure/another.png) 

```r

# plot studentized residuals vs predicted lapply(models, function(x) {
# plot(x, resid(., scaled=T) ~ fitted(.), abline = 0, main=paste('plot of ',
# getrespname(x), ' scaled residuals'), xlab='predicted', ylab='scaled
# residuals') })

```



```r
# box plots of something.. doesn't really make sense..
# plot(DTHdata.random.line.rep, Line ~ resid(., scaled=TRUE)) this doesn't
# work... plot(DTHdata.random.line.rep, id=0.05, idLabels=DTHdata$Line)

# check something with residual from model...
# summary(lm(abs(resid(DTHdata.random.line.rep)) ~
# fitted(DTHdata.random.line.rep)))
# plot(jitter(fitted(DTHdata.random.line.rep)),
# resid(DTHdata.random.line.rep), xlab='Fitted', ylab='Residuals')

# get cook's distance and dfbetas blah = models[[2]] blah2 = influence(blah,
# obs = T) # or (blah, group='Line') ?  cooks.blah2 <- cooks.distance(blah2)
# ME.cook(blah2, plot=TRUE, cutoff=.17) dfbetas.blah2 <- dfbetas(blah2)
# plot(blah2, which='cook', sort=F) # combine cook's D with original df..
# when not NA..  datawcooks = cbind(cooks.blah2, alldata[!is.na(alldata[7]),
# c(1:5,7),]) # plot and label...  dotplot(Line ~ cooks.blah2,
# data=datawcooks, panel = function(x, y, ...) { panel.dotplot(x, y, ...)
# id <- cooks.blah2 > .0002 panel.text(datawcooks$cooks.blah2[id],
# datawcooks$Line[id], labels = datawcooks$Line[id], pos = 4) }) plot(blah2,
# which='dfbetas')
```



```r
################################################################################### model phenotypes with line and rep fixed ... to get LSmeans

# NOTE: These results will not match up with the same procedures on the same
# data, run in SPSS (Linear Mixed Models) or run in SAS (PROC MIXED);
# although SPSS and SAS match one another. It has been discovered that the
# descrepancy is due to different reference coding of the categorical
# variables when in SPSS and SAS compared to the 'lme4' package and 'lmer'
# function. Essentially, all the R functions used here (in this script) code
# categorical factors / variables so that the reference category is the
# category with the LOWEST numerical value (or alphabetically first letter).
# SPSS and SAS both use the opposite strategy; they code categorical factors
# / variables so that the reference category is the category with the
# HIGHEST numerical value (or alphabetically last letter). This is important
# to note because, the SPSS/SAS Mixed Effects model output produces an
# intercept term for the fixed effects which is substantially different from
# the intercept term for the fixed effects produced by the 'lme4' package;
# and of course, with different intercepts comes different predicted values
# based on the model. If interested in getting SPSS or SAS output to match
# what is produced by this script, then simply reverse code the values of
# the categorical variables when the data is imported to SPSS or SAS.
# Meaning, for instance with the class variable; any case with a value of
# 'a' would be changed to a value of 'd' and vice versa, any case with a
# value of 'c' would be changed to a value of 'b' and vice versa.

# disable scientific notation
options(scipen = 999)

# a*b means a + b + a*b a:b means a + b + a:b ....  so * and : are the
# same..  -1 or 0 means no intercept..

# get fixed effect models.. line and rep fixed..
fixedmodels = lapply(varlist, function(x) {
    lm(substitute(i ~ Line + rep, list(i = as.name(x))), data = alldata)
})

# name models in list
names(fixedmodels) = varlist

################################### updated for lsmeans version 2 see ref.grid class definition here around
################################### page 22 http://cran.r-project.org/web/packages/lsmeans/lsmeans.pdf

# get lsmeans fixedmodels.lsmeans = lapply(fixedmodels, function(x) {
# lsmeans(x, ~ Line)}) convert list to dfs
listlength = length(fixedmodels.lsmeans)
```

```
## Error: object 'fixedmodels.lsmeans' not found
```

```r
# one way for (i in 1:listlength) {
# assign(paste0('lsmeansdf',getrespname(fixedmodels[[i]])),
# summary(fixedmodels.lsmeans[[i]])) } another way define empty df
lsmeansdf = as.data.frame(setNames(replicate(7, numeric(0), simplify = F), c("Line", 
    "lsmeans", "SE", "df", "lowerCI", "upperCI", "resp")))
i = 1
# this error is ok.. just ignore it... Error in make.link(misc$tran) : ‘+’
# link not recognised
for (i in 1:listlength) {
    lsmeansdf = rbind(lsmeansdf, cbind(summary(fixedmodels.lsmeans[[i]]), list(resp = getrespname(fixedmodels[[i]]))))
}
```

```
## Error: object 'listlength' not found
```

```r

# dcast it
lsmeanswide = dcast(lsmeansdf, Line ~ resp, value.var = "lsmean")
```

```
## Error: dims [product 1] do not match the length of object [0]
```

```r
# rename cols
varlistlsmeans = lapply(varlist, function(x) {
    paste(x, "_LSmeans", sep = "")
})
names(lsmeanswide) = c("Line", varlistlsmeans)
```

```
## Error: object 'lsmeanswide' not found
```

```r
################################### 

# combine blups and lsmeans
alldataestimates = merge(lsmeanswide, alldataBLUPs, by = "Line")
```

```
## Error: object 'lsmeanswide' not found
```

```r
head(alldataestimates)
```

```
## Error: error in evaluating the argument 'x' in selecting a method for function 'head': Error: object 'alldataestimates' not found
```

```r
# export
allestimatesprefix = paste("sugarBLUPsandLSmeans_", format(Sys.time(), "%Y%m%d_%H%M"), 
    sep = "")
# write.table(alldataestimates, file=paste(allestimatesprefix, 'tsv',
# sep='.'), sep='\t', quote=F, row.names=F)
```



```r
colnames(alldataestimates)[colnames(alldataestimates) == "Line"] = "ID"
```

```
## Error: object 'alldataestimates' not found
```

```r
rqtlfileprefix = paste("sugarphenosRqtl_", format(Sys.time(), "%Y%m%d_%H%M"), 
    sep = "")
# write.csv(alldataestimates, file=paste(rqtlfileprefix, 'csv',
# sep='.'),row.names=F)
```

