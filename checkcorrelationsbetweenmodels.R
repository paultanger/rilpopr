###################################################################################
# check correlation between models
###################################################################################

# what is the correlation between the blups from the two models?
forcorr = as.matrix(DTHdata.random.line.BLUPs[1])
forcorr2 = as.matrix(DTHdata.random.line.rep.BLUPs[1])
colnames(forcorr2) = "DTHlinerepmodel"
forcorrfinal = cbind(forcorr, forcorr2)
# the actual BLUP estimates are different... with rep more normally distributed..
cor(forcorrfinal)
plot(forcorrfinal)
hist(DTHdata.random.line.BLUPs$DTH)
hist(DTHdata.random.line.rep.BLUPs$DTH)
install.packages("ggplot2")
library(ggplot2)
# look at them in one graph
# make new dfs
line = DTHdata.random.line.BLUPs
linerep = DTHdata.random.line.rep.BLUPs
# add factor var
line$model <- 'line'
linerep$model <- 'linerep'
#and combine into your new data frame
bothmodels <- rbind(line, linerep)
# plot it
ggplot(bothmodels, aes(DTH, fill = model)) + geom_density(alpha = 0.2) + theme_bw()