library(qtl)
# get functions
datapath = "~/Desktop/Dropbox/working folder/Rprojects/RILpop/data/"
sourcepath = "~/Desktop/Rgit/rilpopr/"

setwd(sourcepath)
source("myfunctions.R")
source("SetChrOrderToRef.R")
setwd(datapath)

load("modeldfHTP_20150616_1549.Robject")
modeldfHTP = modeldfHTP
# remove epistatic QTL
modeldfHTP = modeldfHTP[!is.na(modeldfHTP$chromosome),]
# change from factors to numbers
modeldfHTP$chromosome = as.character(modeldfHTP$chromosome)
modeldfHTP$lowCIpos = as.numeric(as.character(modeldfHTP$lowCIpos))
modeldfHTP$hiCIpos = as.numeric(as.character(modeldfHTP$hiCIpos))

# get final manual phenotype QTLs

# load("modeldfAllFinalPhenosWithGrainHI_20151127_1630.RObject")
# that is wrong
# try this.. it has grain and HI but the funky results for biomass, height, DTH
load("stepwiseDFGrainHI_20151127_1729.RObject")
# this also works I think but it doesn't have grain and HI..
load("FinalLSmeansstepwiseDFafterfixchr_20150624_1332.RObject")

# so extract grain and HI and add to the old file:
phes = c("grain_rep3", "HI_rep3")

modeldf = modeldf[(modeldf$phenotype %in% phes),]

# add to biomass, height, DTH
FINALmodeldf = rbind(lsmeansmodeldf,modeldf)

head(FINALmodeldf)
# remove epistatic
FINALmodeldf = FINALmodeldf[!is.na(FINALmodeldf$chromosome),]
# remove DTF
FINALmodeldf = FINALmodeldf[!(FINALmodeldf$phenotype == "DTF_LSmeans"),]

# merge them together
as.data.frame(colnames(modeldfHTP))
as.data.frame(colnames(FINALmodeldf))

# remove unnecessary columns
modeldfHTP = modeldfHTP[,-1]

# just keep HTP we want
htps<-c("NDRE", "NDVI",  "Chla", "HTPheight", "CTD")
modeldfHTP = modeldfHTP[modeldfHTP$phenotype %in% htps ,]

# ugh, for now, do this in excel...
# export them

filename = addStampToFilename("HTP_QTL", "tsv")
write.table(modeldfHTP, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)

filename = addStampToFilename("FinalQTL", "tsv")
write.table(FINALmodeldf, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)



# allQTL = rbind(lsmeansmodeldf, FINALmodeldf)

# check that I get the same answer as brook:
# setwd("~/Desktop/Dropbox/AswIR64_QTL_manuscript/shared_AswIR64_QTL_manuscript/rice_rerun/")
# load("crossFinalLSmeansOrderFixedReRunCalcGeno_20161002_newHI.Robject")
# setwd(datapath)
# 
# phes<-c("DTF_LSmeans", "DTH_LSmeans", "biomass_LSmeans", "height_LSmeans", "grain_rep3", "HI_rep3")
# s1s<-scanone(crossH, method="hk", phe=phes)
# not easy to reconcile these..

# give QTL names
# format: q then 2-5 cap letter for trait then chr ID then period then unique ID (so use peak pos)
# lsmeansmodeldf$PhenoID = sapply(strsplit( as.character(lsmeansmodeldf$phenotype) , "_"), "[[", 1)
# lsmeansmodeldf$PhenoID[lsmeansmodeldf$PhenoID == "biomass"] = "BMS"
# lsmeansmodeldf$PhenoID[lsmeansmodeldf$PhenoID == "height"] = "PHT"
# # lsmeansmodeldf$PhenoID[lsmeansmodeldf$PhenoID == "DTF"] = "FDN" # not sure about proper abbrev
# 
# lsmeansmodeldf$QTLname = paste0("q", lsmeansmodeldf$PhenoID, lsmeansmodeldf$chromosome, ".", round(lsmeansmodeldf$position))
# 
# # save it again
# lsmeansmodeldfFINAL = lsmeansmodeldf[, c(26,2:4,7,8,12,23,24)]
# filename = addStampToFilename("QTLsummaryForHei", "csv")
# write.table(lsmeansmodeldf, file=filename, col.names=T, row.names=F, sep=",", quote=FALSE)

# ### get genes #####
# ###################
# 
# # first get gff file with genes from MSU..
# fileurl = "ftp://ftp.plantbiology.msu.edu/pub/data/Eukaryotic_Projects/o_sativa/annotation_dbs/pseudomolecules/version_7.0/all.dir/all.gff3"
# download.file(fileurl, "msu7.gff3")
# setwd("~/Desktop/Dropbox/working folder/Rprojects/RILpop/data/")
# msu7gff = read.delim("msu7.gff3", header=F, comment.char="#")
# # # proper names
# colnames(msu7gff) = c("seqid", "source", "type", "start", "end", "score", "strand", "phase", "attributes")
# msu7gff$seqid = as.character(msu7gff$seqid)
# # # maybe a faster way to do this?
# split = strsplit(msu7gff$seqid, "Chr")
# msu7gff$chr = sapply(split, "[", 2)
# # # only keep genes
# msu7gff = msu7gff[msu7gff$type == "gene",]
# # # and get the gene ID
# split = strsplit(as.character(msu7gff$attributes), ";")
# split2 = sapply(split, "[", 1)
# split3 = strsplit(split2, "ID=")
# msu7gff$gene.id = sapply(split3, "[", 2)
# 
# # get genes
# # as.data.frame(colnames(modeldfHTP))
# # i = 1
# # gff.in <- msu7gff[msu7gff$chr == modeldfHTP[i,"chromosome"] & 
# #                     msu7gff$end <= modeldfHTP[i,"hiCIflankingBP"] & 
# #                     msu7gff$start >= modeldfHTP[i,"lowCIflankingBP"] , ]
# 
# # modeldfHTP[1,c(5,6,24,25)]
# # summary(gff.in)
# 
# # this takes a while.. maybe faster with a matrix or something
# genelist = data.frame()
# # for each QTL
# 
# # just for Chr 3
# lsmeansmodeldf_chr3 = lsmeansmodeldf[37,]
# lsmeansmodeldfbackup = lsmeansmodeldf
# lsmeansmodeldf = lsmeansmodeldf_chr3
# 
# for (i in 1: length(rownames(lsmeansmodeldf))){
#   # put list of genes in df
#   # where the chr are the same, and range of bp match
#   gff.in <- msu7gff[msu7gff$chr == lsmeansmodeldf[i,"chromosome"] & 
#                       msu7gff$end <= lsmeansmodeldf[i,"hiCIflankingBP"] & 
#                       msu7gff$start >= lsmeansmodeldf[i,"lowCIflankingBP"] , ]
#   # if empty, skip
#   if(dim(gff.in)[1] == 0){print ("skipping");next}
#   # otherwise add QTL data
#   gff.in = cbind(gff.in, lsmeansmodeldf[i, c(24, 2:3,6,7,11, 22, 23)])
#   # and combine into big df
#   genelist = rbind(genelist, gff.in)
# }
# 
# # save it
# filename = addStampToFilename("CompleteGeneListChr3_QTL_ForHei", "tsv")
# write.table(genelist, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)
# filename = addStampToFilename("CompleteGeneListFinalLSmeansQTL", "Robject")
# save(genelist, file=filename)
# 
# # filter and reorganize it
# genelistfinal = genelist
# as.data.frame(colnames(genelistfinal))
# # genelistfinal = genelistfinal[,c(13,12,14:18,11,9)]
# genelistfinal = genelistfinal[,c(12:19,11,9)]
# 
# 
# # parse the attributes
# genelistfinal$GO = sapply(strsplit(as.character(genelistfinal$attributes), ";Note="), "[[", 2)
# 
# # fix url encoding http://www.w3schools.com/tags/ref_urlencode.asp
# genelistfinal$GOfixed = gsub("%20", " ", genelistfinal$GO )
# genelistfinal$GOfixed = gsub("%2C", ",", genelistfinal$GOfixed )
# genelistfinal$GOfixed = gsub("%2F", "/", genelistfinal$GOfixed )
# genelistfinal$GOfixed = gsub("%2F", "*", genelistfinal$GOfixed )
# genelistfinal$GOfixed = gsub("%2B", "+", genelistfinal$GOfixed )
# genelistfinal$GOfixed = gsub("%3B", ";", genelistfinal$GOfixed )
# genelistfinal$GOfixed = gsub("%3A", ":", genelistfinal$GOfixed )
# genelistfinal$GOfixed = gsub("%2A", "*", genelistfinal$GOfixed )
# genelistfinal$GOfinal = gsub("%27", "'", genelistfinal$GOfixed )
# 
# # genelistfinal = genelistfinal[,-c(9:11)]
# genelistfinal = genelistfinal[,-c(10:12)]
# 
# 
# filename = addStampToFilename("CompleteGeneList_Chr3_QTLFormatted", "tsv")
# write.table(genelistfinal, file=filename, col.names=T, row.names=F, sep="\t", quote=FALSE)
# 
# filename = addStampToFilename("CompleteGeneListFinalLSmeansQTLFormatted", "Robject")
# save(genelistfinal, file=filename)
