# this is the basic script but replace pheno with each pheno in pheno list.. and replace jobID with a number (1-246)

# load qtl library
library(qtl)
# load the parallel package to utilize multiple threads
library(parallel)

# get home dir
home = Sys.getenv("HOME")

# get scratch dir for data
scratch = Sys.getenv("GSCRATCH")

# hopper
sourcepath = paste0(home, "/rilpopr/")
datapath = paste0(scratch, "/perms_output/")
inputpath = paste0(scratch, "/inputfiles/")

# mostly for file naming function
setwd(sourcepath)
source("myfunctions.R")
setwd(inputpath)

# set variables
perms = batchperms # this gets replaced
method = "hk"
threads = 14 # seems like 16 is ok, but just to be safe.. 

# load the cross and list of phenos
load("crossallphenosOrderFixedReRunCalcGeno_20150612_1559.Robject")
load("phenoswithscan1QTLalpha0.1_20150528_1728.Robject") 

# set random seed
set.seed(jobID) # jobID gets replaced

# run scan2 perm
ptm <- proc.time()
phenoperm = scantwo(cross, pheno.col="phenotorun", method=method, n.perm=perms, n.cluster=threads, addcovar=NULL, verbose=T) # phenotorun gets replaced
proc.time() - ptm

# save it
# set output dir
setwd(datapath)
filename = addStampToFilename(paste0("allphenosscan2_10kperms_", "phenotorun"), "Robject") # phenotorun gets replaced
save(phenoperm, file=filename)
