###################################################################################
# try with nlme instead of lme4
###################################################################################

# try with nlme instead...
#install.packages("nlme")
# don't load at the same time as lme4 !!!!
detach(package:lme4)
library(nlme)

# simple model

# remove with na..
#DTHdataNoNA = DTHdata[complete.cases(DTHdata),]

# line and rep both random
nlmemodel = lme(DTH ~ Line + rep, random = ~ 1 | Line, data=DTHdata, na.action=na.omit)

# line, rep, line rep interaction.. all fixed
nlmemodel = lme(DTH ~ Line*rep, random = ~ 1, data=DTHdata, na.action=na.omit)

# line, rep, line rep interaction.. one random, one fixed
nlmemodel = lme(DTH ~ Line*rep, random = list(Line = ~ 1), data=DTHdata, na.action=na.omit)

# rep is nested in line... all random
nested = lme(DTH ~ rep, random = ~ 1| Line/rep, data=DTHdata, na.action=na.omit)

summary(nested)

# residuals
resid(model)

# CI for fixed and variances
intervals(model)

# BLUPs
fitted(model)

# LSmeans
nlmemodel = lsmeans(model, ~ Line*rep)

# variance components
VarCorr(z) 

# AIC
AIC(z)             # Akaike Information Criterion

# BIC
AIC(z, k = log(n)) # Bayesian Information Criterion (n is the sample size)

BIC(z)             # Bayesian Information Criterion

# spatial stuff
# like this in SAS:
# http://stats.stackexchange.com/questions/45916/how-to-specify-in-r-spatial-covariance-structure-similar-to-sas-sppow-in-a-mar
# repeated /subject=rep type=sp(powa) (row col);
# http://www.ats.ucla.edu/stat/r/faq/spatial_regression.htm
# from nlme.. or try 
gls(CD4t~T, data=df, na.action = (na.omit), method = "REML",
    corr=corCAR1(form=~T|NUM_PAT))

# add correlation structure for lme
# cor=corAR1(0.6,form=~1|Subject)
#pdBlocked ...