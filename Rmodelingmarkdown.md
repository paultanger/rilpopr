
```r

################################################################################### model phenotypes with line and rep random ... no spatial correction

# possible models: just line lmer(pheno ~ + (1 | Line), data=data) line and
# rep lmer(pheno ~ + (1 | Line) + (1 | rep), data=data) line nested in rep
# lmer(pheno ~ + (1 | Line/rep), data=data) note that nesting lmer is not
# necessary if explicit variable names are used lmer tries to detect nesting
# and account for it in the code..  see page 30:
# http://cran.r-project.org/doc/Rnews/Rnews_2005-1.pdf or here:
# http://r.789695.n4.nabble.com/Need-help-with-lmer-model-specification-syntax-for-nested-mixed-model-td3020895.html
# lmer can determine the crossed or nested structure from the data whenever
# the data reflect the structure.  Implicitly nested factors don't reflect
# the structure of the data and rely on external information to augment the
# data given.  The computational methods used in lmer don't depend on
# whether the grouping factors for the random effects are nested or not.
# However they do require that the grouping factors are well-defined.

# ways to model multiple phenotypes:
# http://stackoverflow.com/questions/22134545/how-to-run-a-model-for-multiple-variablescolumns-in-df-with-lmer

############################# function approach linereprandommodel = function(df, varname){
############################# library(lme4) model = lmer(paste0(varname, ' ~ + (1 | Line) + (1 | rep)'),
############################# data=df) return(model) } biomass = linereprandommodel(wtht, 'biomass')
############################# biomass

############################# list approach - store all models in a list and use lapply to do things to
############################# all
```



```r
setwd("C:/Users/paultanger/Desktop/localRgit/RILpop")
```

```
## Error: cannot change working directory
```

```r
source("myfunctions.R")
source("loadlibraries.R")
```



```r
############################# list approach - store all models in a list and use lapply to do things to
############################# all

# just for report output..
setwd("~/Desktop/Rgit/RILpop")
setwd("C:/Users/paultanger/Desktop/localRgit/RILpop")
```

```
## Error: cannot change working directory
```

```r
source("myfunctions.R")
source("loadlibraries.R")
setwd("~/Desktop/Dropbox/working folder/Rprojects/RILpop/data")
setwd("C:\\Users\\paultanger\\Desktop\\Dropbox\\working folder\\Rprojects\\RILpop\\data\\")
```

```
## Error: cannot change working directory
```

```r
alldata = read.delim("DS2012finalphenodataREMOVEOUTLIERS_20140522_0849.tsv")

# rearrange cols to have data together..
alldata = alldata[c(1:6, 10, 7, 11, 8, 12, 9, 13)]

# make list of phenotypes
varlist = names(alldata)[6:13]

# make rep a factor
alldata$rep = as.factor(alldata$rep)

# run random line and rep models
models = lapply(varlist, function(x) {
    lmer(substitute(i ~ +(1 | Line) + (1 | rep), list(i = as.name(x))), data = alldata)
})

# name models in list..
names(models) = varlist

################################################################################### examine models

lapply(models, summary)
```

```
## $DTH
## Linear mixed model fit by REML ['lmerMod']
## Formula: DTH ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 5130 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -4.384 -0.437 -0.057  0.420  4.167 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 61.71    7.86    
##  rep      (Intercept)  5.00    2.24    
##  Residual              9.33    3.05    
## Number of obs: 837, groups: Line, 302; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)    81.67       1.37    59.4
## 
## $logDTH
## Linear mixed model fit by REML ['lmerMod']
## Formula: logDTH ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: -2197 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -4.865 -0.424 -0.048  0.439  4.235 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 0.009421 0.0971  
##  rep      (Intercept) 0.000897 0.0299  
##  Residual             0.001475 0.0384  
## Number of obs: 837, groups: Line, 302; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)   4.3983     0.0182     241
## 
## $biomass
## Linear mixed model fit by REML ['lmerMod']
## Formula: biomass ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 11191 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -2.982 -0.596 -0.058  0.518  3.787 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 1129     33.6    
##  rep      (Intercept)  425     20.6    
##  Residual             1811     42.6    
## Number of obs: 1049, groups: Line, 302; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)    168.8       12.1    13.9
## 
## $logbiomass
## Linear mixed model fit by REML ['lmerMod']
## Formula: logbiomass ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 503.6 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -3.501 -0.539  0.064  0.603  2.530 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 0.0443   0.211   
##  rep      (Intercept) 0.0179   0.134   
##  Residual             0.0666   0.258   
## Number of obs: 1049, groups: Line, 302; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)   5.0706     0.0786    64.5
## 
## $grain
## Linear mixed model fit by REML ['lmerMod']
## Formula: grain ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 8108 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -2.751 -0.631 -0.029  0.570  4.714 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept)  39.3     6.27   
##  rep      (Intercept)  39.0     6.25   
##  Residual             104.3    10.21   
## Number of obs: 1049, groups: Line, 302; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)    37.27       3.64    10.2
## 
## $loggrain
## Linear mixed model fit by REML ['lmerMod']
## Formula: loggrain ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 696.6 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -4.107 -0.550  0.112  0.613  3.316 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 0.0350   0.187   
##  rep      (Intercept) 0.0340   0.184   
##  Residual             0.0877   0.296   
## Number of obs: 1049, groups: Line, 302; rep, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)    3.552      0.107    33.1
## 
## $height
## Linear mixed model fit by REML ['lmerMod']
## Formula: height ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: 5976 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -4.957 -0.431  0.014  0.430  2.851 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 686.6    26.20   
##  rep      (Intercept)  15.9     3.98   
##  Residual             107.2    10.36   
## Number of obs: 687, groups: Line, 302; rep, 2
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)   129.44       3.22    40.2
## 
## $logheight
## Linear mixed model fit by REML ['lmerMod']
## Formula: logheight ~ +(1 | Line) + (1 | rep) 
##    Data: alldata 
## 
## REML criterion at convergence: -631.2 
## 
## Scaled residuals: 
##    Min     1Q Median     3Q    Max 
## -4.887 -0.387  0.033  0.465  2.658 
## 
## Random effects:
##  Groups   Name        Variance Std.Dev.
##  Line     (Intercept) 0.04775  0.2185  
##  rep      (Intercept) 0.00063  0.0251  
##  Residual             0.00676  0.0822  
## Number of obs: 687, groups: Line, 302; rep, 2
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)    4.837      0.022     220
```

```r
lapply(models, AIC)
```

```
## $DTH
## [1] 5138
## 
## $logDTH
## [1] -2189
## 
## $biomass
## [1] 11199
## 
## $logbiomass
## [1] 511.6
## 
## $grain
## [1] 8116
## 
## $loggrain
## [1] 704.6
## 
## $height
## [1] 5984
## 
## $logheight
## [1] -623.2
```

```r
lapply(models, getAICc)
```

```
## $DTH
## Line 
## 5138 
## 
## $logDTH
##  Line 
## -2189 
## 
## $biomass
##  Line 
## 11199 
## 
## $logbiomass
##  Line 
## 511.8 
## 
## $grain
## Line 
## 8116 
## 
## $loggrain
##  Line 
## 704.7 
## 
## $height
## Line 
## 5984 
## 
## $logheight
##   Line 
## -623.1
```

```r
lapply(models, BIC)
```

```
## $DTH
## [1] 5157
## 
## $logDTH
## [1] -2170
## 
## $biomass
## [1] 11219
## 
## $logbiomass
## [1] 531.5
## 
## $grain
## [1] 8135
## 
## $loggrain
## [1] 724.4
## 
## $height
## [1] 6002
## 
## $logheight
## [1] -605.1
```

```r

################################################################################### get stuff from models

# lapply(models, ranef) lapply(models, fixef) lapply(models, logLik)
# lapply(models, coef) lapply(models, resid) this was broken by lme4 version
# 1.1-4 :(
varcomps = lapply(models, varcompsdf)
```

```
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 7.86     61.71   
##  rep      (Intercept) 2.24      5.00   
##  Residual             3.05      9.33   
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 0.0971   0.009421
##  rep      (Intercept) 0.0299   0.000897
##  Residual             0.0384   0.001475
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 33.6     1129    
##  rep      (Intercept) 20.6      425    
##  Residual             42.6     1811    
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 0.211    0.0443  
##  rep      (Intercept) 0.134    0.0179  
##  Residual             0.258    0.0666  
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept)  6.27     39.3   
##  rep      (Intercept)  6.25     39.0   
##  Residual             10.21    104.3   
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 0.187    0.0350  
##  rep      (Intercept) 0.184    0.0340  
##  Residual             0.296    0.0877  
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 26.20    686.6   
##  rep      (Intercept)  3.98     15.9   
##  Residual             10.36    107.2   
##  Groups   Name        Std.Dev. Variance
##  Line     (Intercept) 0.2185   0.04775 
##  rep      (Intercept) 0.0251   0.00063 
##  Residual             0.0822   0.00676
```

```r
library(plyr)
varcompsall = ldply(varcomps, data.frame)
# make new column for excel
varcompsall$H2 = varcompsall$varPCT/100
# rename col
colnames(varcompsall)[colnames(varcompsall) == ".id"] = "phenotype"
# round values
varcompsall$H2 = formatC(round(varcompsall$H2, digits = 2), 2, format = "f")
# get rid of extra stuff varcompsall = varcompsall[varcompsall$component ==
# 'Line',] export it
setwd("/Users/paul/Desktop/Dropbox/working folder/Rprojects/RILpop/data/")
```

```
## Error: cannot change working directory
```

```r
varprefix = paste("DS2012VarComps_", format(Sys.time(), "%Y%m%d_%H%M"), sep = "")
# write.table(varcompsall, file=paste(varprefix, 'tsv', sep='.'), sep='\t',
# quote=F, row.names=F)

# get BLUPs
modelblups = lapply(models, function(x) {
    BLUP(x, "Line")
})
# name the elements in the list with the same names as phenotypes
names(modelblups) = varlist
# write to files
blupprefix = paste("BLUPs_linereprandom_", format(Sys.time(), "%Y%m%d_%H%M"), 
    sep = "")
# sapply(names(modelblups), function (x) {write.table(modelblups[[x]],
# file=paste(blupprefix, x, 'tsv', sep='.'), sep='\t', quote=F,
# row.names=F) } ) write.table(alldata, file='fieldphenofinal.tsv',
# col.names=TRUE, row.names=FALSE, sep='\t', quote=FALSE)

# combine blups into one df.. but makes lots of extra cols...
# otherdataBLUPs = do.call(cbind, modelblups)

# or merge them
alldataBLUPs = Reduce(function(x, y) merge(x, y, by = "Line"), modelblups)

# export write.table(alldataBLUPs, file=paste(blupprefix, 'tsv', sep='.'),
# sep='\t', quote=F, row.names=F)
```


```r
################################################################################### model phenotypes with line and rep ... with spatial power correction this
################################################################################### isn't implemented in lme4...
################################################################################### http://r.789695.n4.nabble.com/lmer-and-correlation-td849697.html

# diagonal covariance structures: (i.e., enforce mutually uncorrelated
# random effects via syntax like ~ (1 | group)+ (0 + x1 | group) + (0 + x2 |
# group)) unstructured covariance matrices: (i.e. all correlations are
# estimated, ~ (1 + x1 + x2 | group)) partially diagonal, partially
# unstructured covariance (y ~ (1 + x1 | group) + (0 + x2 | group), where
# you would estimate a correlation between the random intercept and random
# slope for x1, but no correlations between the random slope for x2 and the
# random intercept and between the random slope for x2 and the random slope
# for x1).

# variance-covariance structures for the residuals?

# use nlme ??
```



```r
################################################################################### check assumptions works ok, but can't label.. lapply(models, mcp.fnc)

# plot each.. label with response name.. great! lapply(models, function(x)
# {plot(x, main=getrespname(x))})

# how to get lmer object from list: models[[1]]
par(mfrow = c(2, 2))

# get histograms of residuals
lapply(models, function(x) {
    hist(resid(x), breaks = 100, main = paste("histogram of ", getrespname(x), 
        " residuals"), xlab = "residuals")
})
```

![plot of chunk plothist](figure/plothist1.png) ![plot of chunk plothist](figure/plothist2.png) 

```r

# get histograms of scaled residuals
lapply(models, function(x) {
    hist(resid(x, scaled = T), breaks = 100, main = paste("histogram of ", getrespname(x), 
        " scaled residuals"), xlab = "residuals")
})
```

![plot of chunk plothist](figure/plothist3.png) ![plot of chunk plothist](figure/plothist4.png) 




```r

par(mfrow = c(2, 2))

# qqplot of residuals
lapply(models, function(x) {
    qqnorm(resid(x), main = paste("qqplot of ", getrespname(x), " residuals"))
    qqline(resid(x))
})
```

![plot of chunk plotstuff](figure/plotstuff1.png) ![plot of chunk plotstuff](figure/plotstuff2.png) 

```
## $DTH
## NULL
## 
## $logDTH
## NULL
## 
## $biomass
## NULL
## 
## $logbiomass
## NULL
## 
## $grain
## NULL
## 
## $loggrain
## NULL
## 
## $height
## NULL
## 
## $logheight
## NULL
```

```r

# qqplot of scaled residuals
lapply(models, function(x) {
    qqnorm(resid(x, scaled = T), main = paste("qqplot of ", getrespname(x), 
        " scaled residuals"))
    qqline(resid(x, scaled = T))
})
```

![plot of chunk plotstuff](figure/plotstuff3.png) ![plot of chunk plotstuff](figure/plotstuff4.png) 

```
## $DTH
## NULL
## 
## $logDTH
## NULL
## 
## $biomass
## NULL
## 
## $logbiomass
## NULL
## 
## $grain
## NULL
## 
## $loggrain
## NULL
## 
## $height
## NULL
## 
## $logheight
## NULL
```

```r

# plot abs value of res vs fitted.. to see outliers...
lapply(models, function(x) {
    plot(fitted(x), abs(resid(x)), main = paste("plot of ", getrespname(x), 
        " scaled residuals"), xlab = "abs(residuals)", ylab = "fitted")
})
```

![plot of chunk plotstuff](figure/plotstuff5.png) ![plot of chunk plotstuff](figure/plotstuff6.png) 

```
## $DTH
## NULL
## 
## $logDTH
## NULL
## 
## $biomass
## NULL
## 
## $logbiomass
## NULL
## 
## $grain
## NULL
## 
## $loggrain
## NULL
## 
## $height
## NULL
## 
## $logheight
## NULL
```

```r

# observed versus fitted value in order to get this to work, need to run the
# models again and exclude outliers..  run random line and rep models
models2 = lapply(varlist, function(x) {
    lmer(substitute(i ~ +(1 | Line) + (1 | rep), list(i = as.name(x))), data = alldata, 
        na.action = na.exclude)
})

# all subsequent 'plot' calls are actually using plot from lme4.. so doesn't
# respect par..  but it uses lattice, should be able to use lattice tools
# for multiplots..
fittedplots = lapply(models2, function(x) {
    plot(x, reformulate(termlabels = "fitted(.)", response = getrespname(x)), 
        abline = c(0, 1), main = paste(getrespname(x), " obs vs fitted"), xlab = "fitted", 
        ylab = "observed")
})

# plot residuals vs predicted lapply(models, function(x) { plot(x, resid(.)
# ~ fitted(.), abline = 0, main=paste('plot of ', getrespname(x), '
# residuals'), xlab='predicted', ylab='residuals') })

# plot residuals vs predicted (by rep)
residbyrep = lapply(models, function(x) {
    plot(x, resid(.) ~ fitted(.), abline = 0, main = paste("plot of ", getrespname(x), 
        " residuals by rep"), pch = unclass(alldata$rep), xlab = "predicted", 
        ylab = "residuals")
})

# arrange them in a 2x2 grid
do.call(grid.arrange, c(fittedplots[1:4], ncol = 2))
```

![plot of chunk plotstuff](figure/plotstuff7.png) 

```r
do.call(grid.arrange, c(fittedplots[5:8], ncol = 2))
```

![plot of chunk plotstuff](figure/plotstuff8.png) 

```r
do.call(grid.arrange, c(residbyrep[1:4], ncol = 2))
```

![plot of chunk plotstuff](figure/plotstuff9.png) 

```r
do.call(grid.arrange, c(residbyrep[5:8], ncol = 2))
```

![plot of chunk plotstuff](figure/plotstuff10.png) 

```r

# plot studentized residuals vs predicted lapply(models, function(x) {
# plot(x, resid(., scaled=T) ~ fitted(.), abline = 0, main=paste('plot of ',
# getrespname(x), ' scaled residuals'), xlab='predicted', ylab='scaled
# residuals') })

```



```r
# box plots of something.. doesn't really make sense..
# plot(DTHdata.random.line.rep, Line ~ resid(., scaled=TRUE)) this doesn't
# work... plot(DTHdata.random.line.rep, id=0.05, idLabels=DTHdata$Line)

# check something with residual from model...
# summary(lm(abs(resid(DTHdata.random.line.rep)) ~
# fitted(DTHdata.random.line.rep)))
# plot(jitter(fitted(DTHdata.random.line.rep)),
# resid(DTHdata.random.line.rep), xlab='Fitted', ylab='Residuals')

# get cook's distance and dfbetas blah = models[[2]] blah2 = influence(blah,
# obs = T) # or (blah, group='Line') ?  cooks.blah2 <- cooks.distance(blah2)
# ME.cook(blah2, plot=TRUE, cutoff=.17) dfbetas.blah2 <- dfbetas(blah2)
# plot(blah2, which='cook', sort=F) # combine cook's D with original df..
# when not NA..  datawcooks = cbind(cooks.blah2, alldata[!is.na(alldata[7]),
# c(1:5,7),]) # plot and label...  dotplot(Line ~ cooks.blah2,
# data=datawcooks, panel = function(x, y, ...) { panel.dotplot(x, y, ...)
# id <- cooks.blah2 > .0002 panel.text(datawcooks$cooks.blah2[id],
# datawcooks$Line[id], labels = datawcooks$Line[id], pos = 4) }) plot(blah2,
# which='dfbetas')
```



```r
################################################################################### model phenotypes with line and rep fixed ... to get LSmeans

# NOTE: These results will not match up with the same procedures on the same
# data, run in SPSS (Linear Mixed Models) or run in SAS (PROC MIXED);
# although SPSS and SAS match one another. It has been discovered that the
# descrepancy is due to different reference coding of the categorical
# variables when in SPSS and SAS compared to the 'lme4' package and 'lmer'
# function. Essentially, all the R functions used here (in this script) code
# categorical factors / variables so that the reference category is the
# category with the LOWEST numerical value (or alphabetically first letter).
# SPSS and SAS both use the opposite strategy; they code categorical factors
# / variables so that the reference category is the category with the
# HIGHEST numerical value (or alphabetically last letter). This is important
# to note because, the SPSS/SAS Mixed Effects model output produces an
# intercept term for the fixed effects which is substantially different from
# the intercept term for the fixed effects produced by the 'lme4' package;
# and of course, with different intercepts comes different predicted values
# based on the model. If interested in getting SPSS or SAS output to match
# what is produced by this script, then simply reverse code the values of
# the categorical variables when the data is imported to SPSS or SAS.
# Meaning, for instance with the class variable; any case with a value of
# 'a' would be changed to a value of 'd' and vice versa, any case with a
# value of 'c' would be changed to a value of 'b' and vice versa.

# disable scientific notation
options(scipen = 999)

# a*b means a + b + a*b a:b means a + b + a:b ....  so * and : are the
# same..  -1 or 0 means no intercept..

# get fixed effect models.. line and rep fixed..
fixedmodels = lapply(varlist, function(x) {
    lm(substitute(i ~ Line + rep, list(i = as.name(x))), data = alldata)
})

# name models in list
names(fixedmodels) = varlist

################################### updated for lsmeans version 2 see ref.grid class definition here around
################################### page 22 http://cran.r-project.org/web/packages/lsmeans/lsmeans.pdf

# get lsmeans
fixedmodels.lsmeans = lapply(fixedmodels, function(x) {
    lsmeans(x, ~Line)
})
# convert list to dfs
listlength = length(fixedmodels.lsmeans)
# one way for (i in 1:listlength) {
# assign(paste0('lsmeansdf',getrespname(fixedmodels[[i]])),
# summary(fixedmodels.lsmeans[[i]])) } another way define empty df
lsmeansdf = as.data.frame(setNames(replicate(7, numeric(0), simplify = F), c("Line", 
    "lsmeans", "SE", "df", "lowerCI", "upperCI", "resp")))
i = 1
# this error is ok.. just ignore it... Error in make.link(misc$tran) : ‘+’
# link not recognised
for (i in 1:listlength) {
    lsmeansdf = rbind(lsmeansdf, cbind(summary(fixedmodels.lsmeans[[i]]), list(resp = getrespname(fixedmodels[[i]]))))
}

# dcast it
lsmeanswide = dcast(lsmeansdf, Line ~ resp, value.var = "lsmean")
# rename cols
varlistlsmeans = lapply(varlist, function(x) {
    paste(x, "_LSmeans", sep = "")
})
names(lsmeanswide) = c("Line", varlistlsmeans)
```

```r
################################### 

# combine blups and lsmeans
alldataestimates = merge(lsmeanswide, alldataBLUPs, by = "Line")
head(alldataestimates)
```

```
##   Line DTH_LSmeans logDTH_LSmeans biomass_LSmeans logbiomass_LSmeans
## 1 1014       86.92          4.467          186.73              5.173
## 2 1032       66.50          4.195          154.45              5.011
## 3 1034       83.92          4.432          141.88              4.927
## 4 1049       86.42          4.461          131.99              4.877
## 5 1054       78.17          4.358          144.91              4.938
## 6 1055       62.77          4.138           85.52              4.392
##   grain_LSmeans loggrain_LSmeans height_LSmeans logheight_LSmeans DTH_BLUP
## 1         36.41            3.516         111.88             4.716    4.746
## 2         31.29            3.407         123.27             4.813  -14.797
## 3         41.86            3.628          92.17             4.522    1.957
## 4         37.96            3.622         105.08             4.654    4.281
## 5         47.25            3.851          94.52             4.547   -3.417
## 6         26.85            3.213          74.12             4.305  -18.404
##   logDTH_BLUP biomass_BLUP logbiomass_BLUP grain_BLUP loggrain_BLUP
## 1     0.06201        11.69         0.06792    -0.4598      -0.02005
## 2    -0.19800       -11.31        -0.04745    -4.1442      -0.10248
## 3     0.02926       -17.53        -0.09550     2.4345       0.04117
## 4     0.05639       -23.97        -0.12904     0.3655       0.03810
## 5    -0.03885       -18.84        -0.10574     6.9191       0.21088
## 6    -0.25352       -65.70        -0.54316    -7.2253      -0.23927
##   height_BLUP logheight_BLUP
## 1     -16.292       -0.11301
## 2      -5.936       -0.02285
## 3     -34.573       -0.29384
## 4     -22.592       -0.17124
## 5     -33.606       -0.28034
## 6     -53.235       -0.51399
```

```r
# export
allestimatesprefix = paste("DS2012BLUPsandLSmeans_", format(Sys.time(), "%Y%m%d_%H%M"), 
    sep = "")
# write.table(alldataestimates, file=paste(allestimatesprefix, 'tsv',
# sep='.'), sep='\t', quote=F, row.names=F)
```

